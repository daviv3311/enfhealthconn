export default function isEmptyArray(arg) {
  return !(Array.isArray(arg) && arg.length > 0);
}
