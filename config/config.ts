export const config = () => ({
  host: Number(process.env.port),
  database: {
    type: "postgres",
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    logging: false,
    entities: ["dist/**/*.entity.{ts,js}"],
    synchronize: false, // never use TRUE in production!
    autoLoadEntities: true,
  },
});
