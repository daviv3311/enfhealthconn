import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { EnfHealthService } from "./enfHealth.service";

@Module({
  imports: [TypeOrmModule.forFeature()],
  providers: [EnfHealthService],
  controllers: [],
  exports: [EnfHealthService],
})
export class EnfHealthModule {}
