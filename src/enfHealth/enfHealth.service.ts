import { Injectable } from "@nestjs/common";
import { InjectEntityManager } from "@nestjs/typeorm";
import { EntityManager } from "typeorm";
import isEmptyArray from "../../utils/isEmptyArray";

@Injectable()
export class EnfHealthService {
  constructor(
    @InjectEntityManager()
    private entityManager: EntityManager,
  ) {}

  async updateEnfHealthConnection() {
    // const value = "5 minutes";

    const rawData = await this.entityManager.query(
      `SELECT * FROM enfusion_connects WHERE (updated_at < NOW() - INTERVAL '1 minutes' AND enf_conn_status = true)`,
    );

    console.log("rawData", rawData);

    if (!isEmptyArray(rawData)) {
      try {
        await Promise.all(
          rawData.map(async (item) => {
            // const rawData = await this.entityManager.query(
            //   `UPDATE enfusion_connects SET enf_conn_status = false WHERE store_code_uid: item.store_code_uid`,
            // );
            await this.entityManager.update(
              "enfusion_connects",
              { store_code_uid: item.store_code_uid },
              { enf_conn_status: false, updated_at: new Date() },
            );
          }),
        );
      } catch (err) {
        console.log(err);
        return true;
      }
    }
  }
}
