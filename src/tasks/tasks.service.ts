import { Injectable, Logger } from "@nestjs/common";
import { Cron } from "@nestjs/schedule";
import { EnfHealthService } from "../enfHealth/enfHealth.service";

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private enfHealthService: EnfHealthService) {}

  // @Interval(10000)
  // async handleInterval() {
  //   // this.logger.debug("Called every 10 seconds");
  //   return await this.enfHealthService.updateEnfHealthConnection();
  // }

  @Cron("*/10 * * * * *")
  async handleCron() {
    this.logger.debug("Called every 10 secs");
    return await this.enfHealthService.updateEnfHealthConnection();
  }
}
