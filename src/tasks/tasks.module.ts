import { Module } from "@nestjs/common";
import { TasksService } from "./tasks.service";
import { EnfHealthModule } from "../enfHealth/enfHealth.module";

@Module({
  imports: [EnfHealthModule],
  providers: [TasksService],
  exports: [TasksService],
})
export class TasksModule {}
